package com.diberder.todoapp.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.diberder.todoapp.R;
import com.diberder.todoapp.model.Task;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CreateTaskActivity extends AppCompatActivity implements View.OnClickListener{

    static final String TASK_STATUS = "TASK_STATUS";
    static final String TASK_OLD = "TASK_OLD";
    static final String TASK_CREATED = "TASK_CREATED";
    static final String TASK_CHANGED = "TASK_CHANGED";
    static final String TASK_DELETED = "TASK_DELETED";
    private boolean isChanged = false;
    private Calendar calendar;
    private Task modTask = null;

    private Drawable originalDrawable;
    private EditText title;
    private EditText description;
    private EditText date;
    private EditText context;
    private Button saveBtn;
    private Button cancelBtn;
    private Button deleteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_create);

        calendar = Calendar.getInstance();
        title = findViewById(R.id.create_task_title);
        description = findViewById(R.id.create_task_description);
        date = findViewById(R.id.create_task_date);
        context = findViewById(R.id.create_task_context);
        saveBtn = findViewById(R.id.button_save);
        cancelBtn = findViewById(R.id.button_cancel);
        deleteBtn = findViewById(R.id.button_delete);

        deleteBtn.setVisibility(View.GONE);
        originalDrawable = title.getBackground();

        final TimePickerDialog.OnTimeSetListener timeDialog = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);
                SimpleDateFormat simpleDate =  new SimpleDateFormat("HH:mm");
            }
        };

        final DatePickerDialog.OnDateSetListener dateDialog = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat simpleDate =  new SimpleDateFormat("dd/MM/yyyy");
                date.setText(simpleDate.format(calendar.getTime()));
            }
        };

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(
                        CreateTaskActivity.this,
                        dateDialog,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });

        saveBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);

        if(getIntent().getExtras() != null) modTask = (Task) getIntent().getExtras().getSerializable(Task.class.getName());
        if(modTask != null) {
            isChanged = true;
            deleteBtn.setVisibility(View.VISIBLE);
            calendar = Calendar.getInstance();
            calendar.setTime(modTask.getDate());

            title.setText(modTask.getTitle());
            description.setText(modTask.getDesc());
            SimpleDateFormat simpleDate =  new SimpleDateFormat("dd/MM/yyyy");
            date.setText(simpleDate.format(modTask.getDate()));
            simpleDate = new SimpleDateFormat("HH:mm");
            context.setText(modTask.getContext());
        }

        if(isChanged)
            getSupportActionBar().setTitle(getString(R.string.change_task));
        else
            getSupportActionBar().setTitle(getString(R.string.create_task));

    }

    private boolean checkInput() {
        boolean valid = true;

        if(title.getText().toString().isEmpty())
            title.setBackgroundResource(R.drawable.edittext_bg_wrong);
        else
            title.setBackgroundDrawable(originalDrawable);

        if(description.getText().toString().isEmpty()) {
            description.setBackgroundResource(R.drawable.edittext_bg_wrong);
            valid = false;
        }
        else
            title.setBackgroundDrawable(originalDrawable);

        if(date.getText().toString().isEmpty()) {
            date.setBackgroundResource(R.drawable.edittext_bg_wrong);
            valid = false;
        }
        else
            title.setBackgroundDrawable(originalDrawable);
        return valid;
    }

    @Override
    public void onClick(View view) {
        Intent returnIntent = new Intent();
        switch(view.getId()) {
            case R.id.button_save:
                if(!checkInput()) return;

                Task task = new Task(title.getText().toString(),
                        description.getText().toString(),
                        calendar.getTime(),
                        context.getText().toString());
                returnIntent.putExtra(Task.class.getName(), task);
                String status = isChanged ? TASK_CHANGED : TASK_CREATED;
                returnIntent.putExtra(TASK_STATUS, status);
                if(isChanged) returnIntent.putExtra(TASK_OLD, modTask);
                setResult(Activity.RESULT_OK,returnIntent);
                break;

            case R.id.button_cancel:
                setResult(Activity.RESULT_CANCELED, returnIntent);
                break;

            case R.id.button_delete:
                returnIntent.putExtra(TASK_OLD, modTask);
                returnIntent.putExtra(TASK_STATUS, TASK_DELETED);
                setResult(Activity.RESULT_OK,returnIntent);
                break;
        }
        finish();
    }
}
