package com.diberder.todoapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.diberder.todoapp.model.DBConsts.*;

/**
 * TaskDAO
 * Data Access Object
 * Allows to make the link between the java Task class and the DB
 */

public class TaskDAO {
    private SQLiteDatabase db;
    private DBOpener opener;
    SimpleDateFormat typeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    /**
     * Create DAO object
     * @param context activity context
     */
    public TaskDAO(Context context){
        this.opener = new DBOpener(context, DB_NAME, null, VERSION_DB);
    }

    /**
     * Open DB
     */
    public void open(){
        db = opener.getWritableDatabase();
    }

    /**
     * Close DB
     */
    public void close(){
        db.close();
    }

    /**
     * Insert a task
     * @param task
     */
    public void insert(Task task){
        ContentValues values = new ContentValues();
        values.put(ID, task.getId().toString());
        values.put(TITLE, task.getTitle());
        values.put(DESCRIPTION, task.getDesc());
        values.put(DATE, typeFormat.format(task.getDate()));
        values.put(CONTEXT, task.getContext());
        db.insert(TABLE_TASK, null, values);
    }

    /**
     * Update a task
     * @param id task's id
     * @param task
     */
    public void update(UUID id, Task task){
        ContentValues values = new ContentValues();
        values.put(ID, task.getId().toString());
        values.put(TITLE, task.getTitle());
        values.put(DESCRIPTION, task.getDesc());
        values.put(DATE, typeFormat.format(task.getDate()));
        values.put(CONTEXT, task.getContext());
        db.update(TABLE_TASK, values, ID + " = \"" + id.toString() + "\"", null);
    }

    /**
     * Remove a task from the db
     * @param id task's id that you want to remove
     */
    public void remove(UUID id){
        db.delete(TABLE_TASK, ID + " = \""  + id.toString() + "\"", null);
    }

    /**
     * @return a list with all the tasks
     */
    public List<Task> getAll() {
        List<Task> tasks = new ArrayList<>();

        Cursor c = db.rawQuery("select * from " + TABLE_TASK,null);
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                tasks.add(cursorToTask(c));
                c.moveToNext();
            }
        }

        c.close();
        return tasks;
    }

    /**
     * to convert a cursor to a task
     * @param c cursor
     * @return the task which match with the cursor
     */
    private Task cursorToTask(Cursor c){
        if (c.getCount() == 0)
            return null;

        Task task = new Task();
        task.setId(UUID.fromString(c.getString(ID_NUM)));
        task.setTitle(c.getString(TITLE_NUM));
        task.setDesc(c.getString(DESCRIPTION_NUM));
        try {
            task.setDate(typeFormat.parse(c.getString(DATE_NUM)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        task.setContext(c.getString(CONTEXT_NUM));

        return task;
    }

}
