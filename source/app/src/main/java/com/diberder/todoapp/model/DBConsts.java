package com.diberder.todoapp.model;

/**
 * Constants for the database
 */
class DBConsts {

    static final int VERSION_DB = 1;

    static final String DB_NAME = "todolist.db";

    static final String TABLE_TASK = "task";
    static final String ID = "ID";
    static final String TITLE = "title";
    static final String DESCRIPTION = "description";
    static final String DATE = "date";
    static final String CONTEXT = "context";

    static final int ID_NUM = 0;
    static final int TITLE_NUM = 1;
    static final int DESCRIPTION_NUM = 2;
    static final int DATE_NUM = 3;
    static final int CONTEXT_NUM = 4;

}
