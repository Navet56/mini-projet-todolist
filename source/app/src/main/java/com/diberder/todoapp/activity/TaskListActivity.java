package com.diberder.todoapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;

import com.diberder.todoapp.R;
import com.diberder.todoapp.adapter.RecyclerAdapterTask;
import com.diberder.todoapp.model.TaskDAO;
import com.diberder.todoapp.model.Task;

import java.util.List;

/**
 *
 */
public class TaskListActivity extends AppCompatActivity implements RecyclerAdapterTask.OnItemSelectedCallback {

    private static final int CREATE_TASK_REQUEST = 1;
    private static final int MODIFY_TASK_REQUEST = 2;

    private TaskDAO dao;
    private List<Task> tasks;
    private CoordinatorLayout layout;
    private FloatingActionButton actionButton;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        getSupportActionBar().setTitle(getString(R.string.list_task));
        dao = new TaskDAO(this);
        dao.open();

        tasks = dao.getAll();

        actionButton = findViewById(R.id.floatingActionButton);
        recyclerView = findViewById(R.id.recyclerView);
        layout = findViewById(R.id.layout);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskListActivity.this, CreateTaskActivity.class);
                startActivityForResult(intent, CREATE_TASK_REQUEST);
            }
        });

        RecyclerAdapterTask recyclerAdapterTask = new RecyclerAdapterTask(tasks, R.layout.row_task, this,this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recyclerAdapterTask);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setItemViewCacheSize(10);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.task_list_menu, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED)
            return;

        if(requestCode == CREATE_TASK_REQUEST && resultCode == Activity.RESULT_OK) {
            Task task = (Task) data.getSerializableExtra(Task.class.getName());
            tasks.add(task);
            dao.insert(task);
            showSnackbarMessage(getString(R.string.task_created));
            refreshRecyclerView();
        } else if(requestCode == MODIFY_TASK_REQUEST && resultCode == Activity.RESULT_OK) {
            Task task = (Task) data.getSerializableExtra(CreateTaskActivity.TASK_OLD);
            int position = tasks.indexOf(task);

            if(data.getStringExtra(CreateTaskActivity.TASK_STATUS).equals(CreateTaskActivity.TASK_CHANGED)) {
                Task newTask = (Task) data.getSerializableExtra(Task.class.getName());
                tasks.set(position, newTask);
                dao.update(task.getId(), newTask);
                showSnackbarMessage(getString(R.string.task_changed));
            }
            else if(data.getStringExtra(CreateTaskActivity.TASK_STATUS).equals(CreateTaskActivity.TASK_DELETED)) {
                tasks.remove(position);
                dao.remove(task.getId());
                showSnackbarTaskDeleted(position, task);
            }
            refreshRecyclerView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dao.close();
    }

    private void showSnackbarMessage(String message) {
        Snackbar.make(layout,message,Snackbar.LENGTH_LONG).show();
    }

    private void showSnackbarTaskDeleted(final int position, final Task task) {
        Snackbar snackbar = Snackbar.make(layout,getString(R.string.task_deleted),Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.cancel),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tasks.add(position, task);
                        dao.insert(task);
                        refreshRecyclerView();
                        Snackbar.make(layout, getString(R.string.task_restored), Snackbar.LENGTH_SHORT).show();
                    }
                });
        snackbar.show();
    }

    private void refreshRecyclerView() {
        RecyclerAdapterTask recyclerAdapterTask = new RecyclerAdapterTask(tasks, R.layout.row_task, this, this);
        recyclerView.setAdapter(recyclerAdapterTask);
        recyclerView.invalidate();
    }

    @Override
    public void onClickReceiver(int position) {
        Task task = tasks.get(position);
        Intent intent = new Intent(this, CreateTaskActivity.class);
        intent.putExtra(Task.class.getName(), task);
        startActivityForResult(intent, MODIFY_TASK_REQUEST);
    }
}