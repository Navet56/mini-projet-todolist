package com.diberder.todoapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.diberder.todoapp.R;
import com.diberder.todoapp.model.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 RecyclerAdapterTask class
 by Evan DIBERDER
 */

public class RecyclerAdapterTask extends RecyclerView.Adapter<RecyclerAdapterTask.ViewHolder>  implements Filterable {

    private Context context;
    private List<Task> tasks;
    private List<Task> filteredList;
    private final int itemLayout;
    private OnItemSelectedCallback callback;

    public RecyclerAdapterTask(List<Task> t, int layout, Context c, OnItemSelectedCallback callback) {
        this.tasks = t;
        this.filteredList = t;
        this.itemLayout = layout;
        this.context = c;
        this.callback = callback;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final CardView cardView;
        final TextView titleView;
        final TextView descView;
        final TextView dateView;
        final TextView contextView;

        ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.row_task_layout);
            titleView = itemView.findViewById(R.id.title);
            descView = itemView.findViewById(R.id.description);
            dateView = itemView.findViewById(R.id.date);
            contextView = itemView.findViewById(R.id.context);
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Task task = filteredList.get(position);
        holder.itemView.setTag(position);
        holder.titleView.setText(task.getTitle());
        holder.descView.setText(context.getString(R.string.description) + " : " + task.getDesc());
        holder.dateView.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(task.getDate()));
        holder.contextView.setText(context.getString(R.string.context) + " : " + task.getContext());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onClickReceiver(position);
                }
            }
        });
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String constraintString = constraint.toString().toLowerCase();
                if (constraintString.isEmpty()) {
                    filteredList = tasks;
                } else {
                    List<Task> filteredList = new ArrayList<>();
                    for (Task  t : tasks) {
                        if (t.getContext().toLowerCase().contains(constraintString)) {
                            Log.w(this.getClass().getName(), "performFiltering: found task = " + t.getTitle());
                            filteredList.add(t);
                        }
                    }
                    RecyclerAdapterTask.this.filteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<Task>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemSelectedCallback {
        void onClickReceiver(int position);
    }
}
