package com.diberder.todoapp.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.diberder.todoapp.model.DBConsts.*;

/**
 * DataBase Open Helper
 * allows to create the database and open it
 */
public class DBOpener extends SQLiteOpenHelper {
    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_TASK + " ("
            + ID + ", " + TITLE + ", " + DESCRIPTION + ", "
            + DATE + "," + CONTEXT + ");";


    DBOpener(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE " + TABLE_TASK + ";");
        onCreate(sqLiteDatabase);
    }


}
