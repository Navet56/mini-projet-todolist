package com.diberder.todoapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Class to represent a task
 */
public class Task implements Serializable {
    private UUID id;
    private String title;
    private String desc;
    private Date date;
    private String context;

    public Task() {
        this.id = UUID.randomUUID();
    }

    public Task(String title, String desc, Date date, String context) {
        this.id = UUID.randomUUID();
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.context = context;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) && (getClass() == obj.getClass()) && ((Task) obj).id.equals(id);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc= desc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

}
